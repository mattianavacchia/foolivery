<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<!-- content -->
<?php 
	$total_price = 0;
	$total_item = 0;
?>
<section>
	<h1 class="text-center">Carrello</h1>
	<div class="container">
		<div class="row" id="row-cart-page">
			<div class="table-responsive" id="order_table">
				<table class="table table-bordered table-striped">
					<tr>  
			            <th>Nome</th>  
			            <th>Quantità</th>  
			            <th>Prezzo</th>  
			            <th>Totale</th>  
			            <th>Azioni</th>  
			        </tr>
			<?php				
				if(!empty($_SESSION["shopping_cart"])) {
					
					foreach($_SESSION["shopping_cart"] as $keys => $values) {?>
						<tr>
							<td><?php echo($values["product_name"]); ?></td>
							<td><?php echo($values["product_quantity"]); ?></td>
							<td style="text-align: right;"><?php echo($values["product_price"]); ?></td>
							<td style="text-align: right;"><?php echo(number_format($values["product_quantity"] * $values["product_price"], 2)); ?></td>
							<td><button name="delete" class="btn btn-danger btn-xs delete delete-product" id="<?php echo($values["product_id"]); ?>">Remove</button></td>
						</tr>
						<?php
							$total_price = $total_price + ($values["product_quantity"] * $values["product_price"]);
							$total_item = $total_item + 1;
					}
					?>
					<tr>  
				        <td colspan="3" style="text-align: right;">Total</td>  
				        <td style="text-align: right;"><?php echo(number_format($total_price, 2)); ?></td>  
				        <td></td>  
				    </tr>
				    <?php
				}
				else {
					?>
				    <tr>
				    	<td colspan="5" style="text-align: center;">
				    		Il tuo carrello è vuoto.
				    	</td>
				    </tr>
				    <?php
				}
			?>
				</table>
			</div>
		</div>
		<div class="row" style="margin-bottom: 20px;">
			<?php if ($total_item == 0) : ?>
			<?php else: ?>
				<a style="float: right;" href="./checkout.php"><p class="btn btn-primary">Checkout</p></a>
				<p style="float: right; margin-right: 10px;" id="clear_cart" class="btn btn-primary" data-dismiss="modal">Svuota Carrello</p>
			<?php endif; ?>
		</div>
	</div>
</section>

<?php include("includes/footer.php"); ?>
