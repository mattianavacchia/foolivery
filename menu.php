<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<!-- content -->

<?php

	if (isset($_POST["searchfor"])) {
		$searchFor = $_POST["searchfor"];
	}	
	
?>

<section id="menu">
	<h1 class="text-center">I Delizioni Menu</h1>
	<div class="container">
		<div class="row">
			<?php if (isset($_POST["searchfor"])) : ?>
				<p class="text-center">Stai ricerdando per <?php echo($searchFor); ?></p>
			<?php endif; ?>
			<ul class="nav nav-pills">
				<li class="active"><a data-toggle="pill" href="#home">Piatto Carne</a></li>
			  	<li><a data-toggle="pill" href="#menu1">Piatto Pesce</a></li>
			  	<li><a data-toggle="pill" href="#menu2">Piatto Vegetariano</a></li>
			  	<li><a data-toggle="pill" href="#menu3">Piatto Vegano</a></li>
			</ul>
			<div class="tab-content">
			  	<div id="home" class="tab-pane fade in active text-center">
			    	<?php
				    	if (isset($_POST["searchfor"])) {
					    	$query_sql="SELECT * FROM pietanza WHERE pietanza_tipo = 1 AND nome LIKE '%" . $searchFor . "%' OR descrizione LIKE '%" . $searchFor . "%'";
				    	} else {
					    	$query_sql="SELECT * FROM pietanza WHERE pietanza_tipo = 1";
				    	}
						$result = $mysqli->query($query_sql);
						if ($result !== false) {
							if ($result->num_rows > 0) {
								while($row = $result->fetch_assoc()) {?>
									<div class="col-md-6 menu-dish">
							    		<div class="dish text-center">
						                    <img src="images/menu-img/dish-1.png" alt="">
						                    <div class="dish-info">
						                        <h2 class="dish-name"><?php echo $row["nome"]; ?> <span class="dish-price"><?php echo $row["prezzo"]; ?>€</span></h2>
						                    </div>
						                    <button id="button-info" data-nome="<?php echo $row["nome"]; ?>" data-descrizione="<?php echo $row["descrizione"]; ?>" class="text-uppercase btn-special" data-toggle="modal" data-target="#modal-info"><i class="fas fa-info-circle"></i> Info</button>
						                    <?php if(isset($_SESSION['type'])) : ?>
												<button id="<?php echo $row["id"]; ?>" data-price="<?php echo $row["prezzo"]; ?>" data-name="<?php echo $row["nome"]; ?>" class="add_to_cart text-uppercase btn-special"><i class="fas fa-shopping-cart"></i> AGGIUNGI</button>
											<?php else: ?>
												<button class="text-uppercase btn-special" onclick="snackBar('Esegui il login per aggiungere elementi al carrello.')"><i class="fas fa-shopping-cart"></i> AGGIUNGI</button>
											<?php endif; ?>
						                </div>
							    	</div>
								<?php }
							} else {?>
								<p class="text-center" style="font-size: 2em;">Non ci sono pietanze.</p>
							<?php }
						}
					?>
			  	</div>
			  	<div id="menu1" class="tab-pane fade">
			    	<?php
						if (isset($_POST["searchfor"])) {
					    	$query_sql="SELECT * FROM pietanza WHERE pietanza_tipo = 2 AND nome LIKE '%" . $searchFor . "%' OR descrizione LIKE '%" . $searchFor . "%'";
				    	} else {
					    	$query_sql="SELECT * FROM pietanza WHERE pietanza_tipo = 2";
				    	}
				    	$result = $mysqli->query($query_sql);
						if ($result !== false) {
							if ($result->num_rows > 0) {
								while($row = $result->fetch_assoc()) {?>
									<div class="col-md-6 menu-dish">
							    		<div class="dish text-center">
						                    <img src="images/menu-img/dish-1.png" alt="">
						                    <div class="dish-info">
						                        <h2 class="dish-name"><?php echo $row["nome"]; ?> <span class="dish-price"><?php echo $row["prezzo"]; ?>€</span></h2>
						                    </div>
						                    <button id="button-info" data-nome="<?php echo $row["nome"]; ?>" data-descrizione="<?php echo $row["descrizione"]; ?>" class="text-uppercase btn-special" data-toggle="modal" data-target="#modal-info"><i class="fas fa-info-circle"></i> Info</button>
						                    <?php if(isset($_SESSION['type'])) : ?>
												<button id="<?php echo $row["id"]; ?>" data-price="<?php echo $row["prezzo"]; ?>" data-name="<?php echo $row["nome"]; ?>" class="add_to_cart text-uppercase btn-special"><i class="fas fa-shopping-cart"></i> AGGIUNGI</button>
											<?php else: ?>
												<button class="text-uppercase btn-special" onclick="snackBar('Esegui il login per aggiungere elementi al carrello.')"><i class="fas fa-shopping-cart"></i> AGGIUNGI</button>
											<?php endif; ?>
						                </div>
							    	</div>
								<?php }
							} else {?>
								<p class="text-center" style="font-size: 2em;">Non ci sono pietanze.</p>
							<?php }
						}
					?>
			  	</div>
			  	<div id="menu2" class="tab-pane fade">
			    	<?php
						if (isset($_POST["searchfor"])) {
					    	$query_sql="SELECT * FROM pietanza WHERE pietanza_tipo = 3 AND nome LIKE '%" . $searchFor . "%' OR descrizione LIKE '%" . $searchFor . "%'";
				    	} else {
					    	$query_sql="SELECT * FROM pietanza WHERE pietanza_tipo = 3";
				    	}
						$result = $mysqli->query($query_sql);
						if ($result !== false) {
							if ($result->num_rows > 0) {
								while($row = $result->fetch_assoc()) {?>
									<div class="col-md-6 menu-dish">
							    		<div class="dish text-center">
						                    <img src="images/menu-img/dish-1.png" alt="">
						                    <div class="dish-info">
						                        <h2 class="dish-name"><?php echo $row["nome"]; ?> <span class="dish-price"><?php echo $row["prezzo"]; ?>€</span></h2>
						                    </div>
						                    <button id="button-info" data-nome="<?php echo $row["nome"]; ?>" data-descrizione="<?php echo $row["descrizione"]; ?>" class="text-uppercase btn-special" data-toggle="modal" data-target="#modal-info"><i class="fas fa-info-circle"></i> Info</button>
						                    <?php if(isset($_SESSION['type'])) : ?>
												<button id="<?php echo $row["id"]; ?>" data-price="<?php echo $row["prezzo"]; ?>" data-name="<?php echo $row["nome"]; ?>" class="add_to_cart text-uppercase btn-special"><i class="fas fa-shopping-cart"></i> AGGIUNGI</button>
											<?php else: ?>
												<button class="text-uppercase btn-special" onclick="snackBar('Esegui il login per aggiungere elementi al carrello.')"><i class="fas fa-shopping-cart"></i> AGGIUNGI</button>
											<?php endif; ?>
						                </div>
							    	</div>
								<?php }
							} else {?>
								<p class="text-center" style="font-size: 2em;">Non ci sono pietanze.</p>
							<?php }
						}
					?>
			  	</div>
			  	<div id="menu3" class="tab-pane fade">
			    	<?php
						if (isset($_POST["searchfor"])) {
					    	$query_sql="SELECT * FROM pietanza WHERE pietanza_tipo = 4 AND nome LIKE '%" . $searchFor . "%' OR descrizione LIKE '%" . $searchFor . "%'";
				    	} else {
					    	$query_sql="SELECT * FROM pietanza WHERE pietanza_tipo = 4";
				    	}
						$result = $mysqli->query($query_sql);
						if ($result !== false) {
							if ($result->num_rows > 0) {
								while($row = $result->fetch_assoc()) {?>
									<div class="col-md-6 menu-dish">
							    		<div class="dish text-center">
						                    <img src="images/menu-img/dish-1.png" alt="">
						                    <div class="dish-info">
						                        <h2 class="dish-name"><?php echo $row["nome"]; ?> <span class="dish-price"><?php echo $row["prezzo"]; ?>€</span></h2>
						                    </div>
						                    <button id="button-info" data-nome="<?php echo $row["nome"]; ?>" data-descrizione="<?php echo $row["descrizione"]; ?>" class="text-uppercase btn-special" data-toggle="modal" data-target="#modal-info"><i class="fas fa-info-circle"></i> Info</button>
						                    <?php if(isset($_SESSION['type'])) : ?>
												<button id="<?php echo $row["id"]; ?>" data-price="<?php echo $row["prezzo"]; ?>" data-name="<?php echo $row["nome"]; ?>" class="add_to_cart text-uppercase btn-special"><i class="fas fa-shopping-cart"></i> AGGIUNGI</button>
											<?php else: ?>
												<button class="text-uppercase btn-special" onclick="snackBar('Esegui il login per aggiungere elementi al carrello.')"><i class="fas fa-shopping-cart"></i> AGGIUNGI</button>
											<?php endif; ?>
						                </div>
							    	</div>
								<?php }
							} else {?>
								<p class="text-center" style="font-size: 2em;">Non ci sono pietanze.</p>
							<?php }
						}
					?>
			  	</div>
			</div>
		</div>
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="modal-info" tabindex="-1" role="dialog" aria-labelledby="modal-info" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Descrizione</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
		      <div class="container">
			      <div class="row">
				      <p id="name-single-product"> </p>
				      <p id="description-single-product"> </p>
			      </div>
		      </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
	      </div>
	    </div>
	  </div>
	</div>
</section>

<?php include("includes/footer.php"); ?>
