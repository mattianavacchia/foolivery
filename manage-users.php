<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<?php if(login_check($mysqli) == true && check_type(1)) : ?>

	<!-- content -->
	<section id="manage-menu">
		<h1 class="text-center">Utenti</h1>
		<div class="container">
			<div class="row text-center" style="background: #e9e9e9; padding: 15px 30px 30px;">
				<p class="text-uppercase">Comandi rapidi:</p><br/>
				<button class="change-state" data-toggle="modal" data-target="#manage-users-add">Aggiungi un nuovo utente</button>
			</div>
			<div class="row table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th id="#" scope="col">#</th>
							<th id="generalita_user" scope="col">Generalità</th>
							<th id="email_user" scope="col">Email</th>
							<th id="ruolo_user" scope="col">Ruolo</th>
							<th id="elimina" scope="col">Elimina</th>
						</tr>
					</thead>
					<tbody>
						<?php
							// id	username	email	password	salt	type
							$query_sql="SELECT * FROM members";
							$result = $mysqli->query($query_sql);
							if ($result !== false) {
								if ($result->num_rows > 0) {
									while($row = $result->fetch_assoc()) {
										if ($_SESSION['user_id'] != $row["id"]) {?>
											<tr>
												<td headers="#" scope="row"><?php echo $row["id"]; ?></td>
												<td headers="generalita_user"><?php echo $row["username"]; ?></td>
												<td headers="email_user"><?php echo $row["email"]; ?></td>
												<td headers="ruolo_user"><?php echo type_from_number($row["type"], $mysqli); ?></td>
												<td headers="elimina"><p data-id="<?php echo $row["id"]; ?>" class="text-uppercase btn-manage btn-manage-remove"><i class="glyphicon glyphicon-remove"></i> Elimina</p></td>
											</tr>
										<?php
										}
									}
								}
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
		
		<!-- Modal -->
		<div class="modal fade" id="manage-users-add" tabindex="-1" role="dialog" aria-labelledby="manage-users-add" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Aggiungi nuovo utente</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
			      <form id="ajax-contact" method="post" action="./api/api-magage-users-add.php">
		            <div>
		                <p><label for="email"><strong>Email</strong>
		                	<input type="text" placeholder="Inserisci Email" name="email" id="email" required>
		                </label></p>
		
		                <p><label for="password"><strong>Password</strong>
		                	<input type="password" placeholder="Inserisci Password" id="password" name="p" required>
		                </label></p>
		                
		                <p><label for="generalita"><strong>Generalità</strong>
		                	<input type="text" placeholder="Inserisci Generalità" name="generalita" id="generalita" required>
		                </label></p>
		
		                <p><label for="type"><strong>Tipologia di account</strong>
			                <select id="type" name="type" size="4" required>
				                <option value="1">Admin</option>
			                    <option value="2">Cliente</option>
			                    <option value="3">Fornitore</option>
			                </select>
		                </label></p>
		                
		                <input class="btn-login" type="submit" value="Registra" onclick="formhash(this.form, this.form.password);"/>
		            </div>
		        </form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
		      </div>
		    </div>
		  </div>
		</div>
	</section>

	<script src="assets/js/sha512.js"></script>
	<script src="assets/js/forms.js"></script>
	
<?php else: ?>

	<section id="manage-menu" class="text-center">
		<h1>Accesso Negato</h1>
		<p>Non hai un account adeguato per poter visualizzare questa pagina.</p>
	</section>
	
<?php endif; ?>

<?php include("includes/footer.php"); ?>
