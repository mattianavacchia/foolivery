<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<!-- content -->
<?php if(isset($_SESSION['type'])) : ?>
	<section>
		<h1 class="text-center">Compila i campi e completa l'acquisto.</h1>
		<div class="container">
			<div class="row">
				<?php
					$total_item = 0;
					$total_price = 0;
					$output = '
					<div class="table-responsive" id="order_table">
						<table class="table table-bordered table-striped">
							<tr>  
					            <th>Nome</th>  
					            <th>Quantità</th>  
					            <th>Prezzo</th>  
					            <th>Totale</th> 
					        </tr>
					';
					
					if(!empty($_SESSION["shopping_cart"])) {
						
						foreach($_SESSION["shopping_cart"] as $keys => $values) {
							$output .= '
							<tr>
								<td>'.$values["product_name"].'</td>
								<td>'.$values["product_quantity"].'</td>
								<td style="text-align: right;">$ '.$values["product_price"].'</td>
								<td style="text-align: right;">$ '.number_format($values["product_quantity"] * $values["product_price"], 2).'</td>
							</tr>
							';
							$total_price = $total_price + ($values["product_quantity"] * $values["product_price"]);
							$total_item = $total_item + 1;
						}
						$output .= '
						<tr>  
					        <td colspan="3" style="text-align: right;">Total</td>  
					        <td style="text-align: right;">$ '.number_format($total_price, 2).'</td> 
					    </tr>
						';
					}
					else {
						$output .= '
					    <tr>
					    	<td colspan="4" style="text-align: center;">
					    		Il tuo carrello è vuoto.
					    	</td>
					    </tr>
					    ';
					}
					$output .= '</table></div>';
					
					echo($output);
					
				?>
				<form style="margin-bottom: 30px;" action="./completed-order.php" method="post" name="checkout_form">
		            <div>
			            <p><label for="generalita"><strong>Generalità</strong>
			                <input type="text" placeholder="Inserisci Generalità per la consegna" value="<?php if(isset($_SESSION['type'])) { echo($_SESSION['username']); } ?>" name="generalita" id="generalita" required>
		                </label></p>
			            
		                <p><label for="email"><strong>Email</strong>
		                	<input type="text" placeholder="Inserisci email" value="<?php if(isset($_SESSION['type'])) { echo($_SESSION['email']); } ?>" name="email" id="email" required>
		                </label></p>
		                
		                <fieldset>
			                <div class="form-group">
				                <div class="radio">
									<p><label><input type="radio" name="optConsegna" value="All'ingresso del campus" checked>All'ingresso del campus</label></p>
								</div>
						    	<div class="radio">
						        	<p><label class="radio-inline control-label">
										<input type="radio" id="altro_consegna" name="optConsegna" value="Altro:">
										Altro:
									</label></p>
						      	</div>
						    </div>
						    <div class="form-group">
							    <p><label for="input_zona"><strong>Zona</strong>
						    		<input placeholder="Zona specifica dell'edificio" type="text" class="form-control" id="input_zona" name="input_zona" data-rule-required="true" disabled>
							    </label></p>
						    </div>
		                </fieldset>
		                
		                
					    <?php if($total_item == 0) : ?>
					    	<input class="btn-login" type="submit" value="Prima di Ordine inserisci qualcosa nel carrello!" disabled />
					    <?php else: ?>
					    	<input class="btn-login" type="submit" value="Ordina" />
					    <?php endif; ?>
		            </div>
		        </form>
			</div>
		</div>
	</section>
<?php else: ?>
	<section>
		<h1 class="text-center">Non hai i permessi per accedere a questa area.</h1>
	</section>
<?php endif; ?>

<?php include("includes/footer.php"); ?>
