<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<?php if(login_check($mysqli) == true && ( check_type(2) )) : ?>

	<!-- content -->
	<section>
		<h1 class="text-center">Notifiche</h1>
		<div class="container">
			<div class="row">
				<div class="row text-center" style="background: #e9e9e9; padding: 15px 30px 30px;">
					<p class="text-uppercase">Comandi rapidi:</p><br/>
					<button id="delete-all-notifications" class="delete">Elimina Tutte</button>
				</div>
				<div style="margin-bottom: 30px;" id="notification-center" class="row">
					<?php
						$query_sql = "SELECT * FROM notifiche_cliente WHERE id_cliente = " .$_SESSION["user_id"];
						$result = $mysqli->query($query_sql);
						if ($result != false) {
							while($row = $result->fetch_assoc()) {
								if ($row['checked'] != 0) {
									$checked = "n-checked";
								} else {
									$checked = "n-unchecked";
								}
							?>

								<div id="single-notification" data-id="<?php echo($row['id_notifica']); ?>" class="notification <?php echo($checked); ?>" role="alert">
									<p><?php echo($row['testo']); ?></p>
									<button class="delete" data-id="<?php echo($row['id_notifica']); ?>">Elimina Notifica</button>
								</div>

							<?php }
						}
					?>
					<?php
						$query_sql = "UPDATE notifiche_cliente SET checked = 1 WHERE id_cliente = " . $_SESSION["user_id"];
						$result = $mysqli->query($query_sql);
					?>
				</div>
			</div>
		</div>
	</section>

<?php else: ?>

	<section id="manage-menu" class="text-center">
		<h1>Accesso Negato</h1>
		<p>Non hai un account adeguato per poter visualizzare questa pagina.</p>
	</section>

<?php endif; ?>

<?php include("includes/footer.php"); ?>
