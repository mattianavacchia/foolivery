<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<?php if(login_check($mysqli) == true) : ?>

	<!-- content -->
	<section>
		<div class="container">
			<div class="row">
				<h1 class="text-center" style="font-size: 3em;">Hai effettuato l'accesso come: <?php echo($_SESSION["username"]); ?>.</h1>
			</div>
		</div>
	</section>
	
<?php else: ?>

	<section id="manage-menu" class="text-center">
		<h1>Accesso Negato</h1>
		<p>Non hai un account adeguato per poter visualizzare questa pagina.</p>
	</section>
	
<?php endif; ?>

<?php include("includes/footer.php"); ?>
