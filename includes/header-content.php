		<header id="header">
			<nav class="navbar navbar-transparent">
			  <div class="container-fluid">
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			    </div>
			    <div class="collapse navbar-collapse" id="myNavbar">
			      <ul class="nav navbar-nav">
				      <li><a href="index.php">Home</a></li>
				      <li><a href="menu.php">Menu</a></li>
				      <?php if(@$_SESSION['type'] == 1) : ?>
				      	<li><a href="manage-suppliers.php">Manage Suppliers</a></li>
				      	<li><a href="manage-users.php">Manage User</a></li>
				      	<!-- ADMIN -->
				      <?php endif; ?>
				      <?php if(@$_SESSION['type'] == 2) : ?>
				      	<!-- CLIENTE -->
				        <li><a data-toggle="modal" data-target="#modalCart"><i class="fas fa-shopping-cart"></i> Carrello (<span id="cart_item_count">0</span>)</a>
				        <!-- Modal -->
				        </li>
				        <li><a href="notifications.php"><i class="fas fa-bell"></i> <span id="notification_count">0</span></a></li>
				      <?php endif; ?>
				      <?php if(@$_SESSION['type'] == 3) : ?>
				      	<!-- FORNITORE -->
								<li><a href="manage-bellboy.php">Manage Bellboy</a></li>
				      	<li><a href="manage-menu.php">Manage Menu</a></li>
				        <li><a href="orders.php">Orders (<span id="order_count">0</span>)</a></li>
				      <?php endif; ?>
				      <?php if(@$_SESSION['type'] == 4) : ?>
				      	<!-- FATTORINO -->
				        <li><a href="deliveries.php"><i class="fas fa-bell"></i> <span id="notification_count">0</span></a></li>
				      <?php endif; ?>
			      </ul>
			      <ul class="nav navbar-nav navbar-right">
			        <?php if(isset($_SESSION['type'])) : ?>
			        	<li><a href="profile.php"><span class="glyphicon glyphicon-user"></span> <?php echo($_SESSION['username']); ?></a></li>
			        	<li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
			        <?php else: ?>
			        	<li><a href="registration.php"><span class="glyphicon glyphicon-user"></span> Registrati</a></li>
						<li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
			        <?php endif; ?>
			      </ul>
			    </div>
			  </div>
			</nav>
		</header>

		<div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="modalCart" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" >Carrello</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div id="cart_details" class="modal-body">

		      </div>
		      <div class="modal-footer">
			    <p class="btn btn-primary"><a href="./checkout.php">Checkout</a></p>
			    <button type="button" id="clear_cart" class="btn btn-primary" data-dismiss="modal">Svuota Carrello</button>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
		      </div>
		    </div>
		  </div>
		</div>

		<?php if (strpos($_SERVER['SCRIPT_NAME'], 'index.php') !== false) : ?>

			<section id="banner-home" class="text-center">
				<div class="container">
					<div class="row row-eq-height">
						<div class="col-12 col-sm-5 welcoming">
							<img src="images/icons/foolivery-icon.png" alt="Foolivery logo" />
							<h1>Benvenuti su <br/><span>Foolivery</span></h1>
							<p class="text-uppercase btn-special"><a href="./menu.php">Sfoglia Menu</a></p>
						</div>
						<div class="col-12 col-sm-7 text-center searchbar" aria-hidden="true">
							<!-- Search form -->
							<h1>Trova il tuo piatto preferito</h1>
							<form method="post" action="./menu.php">
								<p><label>Parola chiave: <input class="form-control form-control-search" type="text" placeholder="Hamburger" name="searchfor" id="searchfor" aria-label="Cerca" /></label></p>
								<input type="submit" class="text-uppercase btn-special" value="CERCA"/>
							</form>
						</div>
					</div>
				</div>
			</section><!-- /#banner-home -->

		<?php else: ?>

			<section id="banner" class="text-center">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<?php $names = explode(".", basename($_SERVER['PHP_SELF'])); ?>
							<h1><a href="index.php">Home</a>/<?php echo($names[0]); ?></h1>
						</div>
					</div>
				</div>
			</section><!-- /#banner -->

		<?php endif; ?>
