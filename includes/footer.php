		
		<footer id="footer" class="text-center">
			<div class="container-fluid">
				<div class="row">
					<div class="information">
						<h6>Get In Touch</h6>
						<p>
							Tel: +39 123 456 7890<br/>
							Indirizzo: Via Macchiavelli 52, Cesena<br/>
							Email: info@foolivery.it<br/>
							Seguici su:
						</p>
						<ul>
							<li><i class="fab fa-facebook-f"></i></li>
							<li><i class="fab fa-instagram"></i></li>
							<li><i class="fab fa-twitter"></i></li>
						</ul>
					</div>
				</div>
				<div class="row copyright">
					<p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved.</p>
				</div>
			</div>
		</footer>
		
		<div id="snackbar"></div>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="dist/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
		<script>
			function snackBar(string) {
			  var x = document.getElementById("snackbar");
			  x.innerHTML = string;
			  x.className = "show";
			  setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
			}
		</script>
		<script src="assets/js/menu.js"></script>
		<script src="assets/js/cart.js"></script>
		<script src="assets/js/checkout.js"></script>
		<!-- FORNITORE -->
		<script src="assets/js/manage-users.js"></script>
		<script src="assets/js/manage-menu.js"></script>
		<script src="assets/js/notifications.js"></script>
		<!--<?php if(@$_SESSION['type'] == 1) : ?>

      		<script src="assets/js/manage-users.js"></script>
	  		<script src="assets/js/manage-menu.js"></script>
		<?php endif; ?>
		<?php if(@$_SESSION['type'] == 2) : ?>
			
			<script src="assets/js/cart.js"></script>
			<script src="assets/js/checkout.js"></script>
			<script src="assets/js/notifications.js"></script>
		<?php endif; ?>
		<?php if(@$_SESSION['type'] == 3) : ?>
			
			<script src="assets/js/manage-users.js"></script>
			<script src="assets/js/manage-menu.js"></script>
			<script src="assets/js/notifications.js"></script>
		<?php endif; ?>
		<?php if(@$_SESSION['type'] == 4) : ?>
			
		<script src="assets/js/notifications.js"></script>
	-->
		<?php endif; ?>*/
	</body>
</html>