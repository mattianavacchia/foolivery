<?php include('config.php'); ?>
<?php include('functions.php'); ?>
<?php sec_session_start(); ?>
<!DOCTYPE html>
<html lang="it">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta name="description" content="">
		<meta name="author" content="">
		
		<title>Foolivery</title>
		
		<!-- Bootstrap core CSS -->
		<link href="dist/css/bootstrap.min.css" rel="stylesheet">
		
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<link href="assets/css/default.css" rel="stylesheet">
		<link href="assets/css/style.css" rel="stylesheet">
		<link href="assets/css/animate.css" rel="stylesheet">

		<!-- GoogleFonts link -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans|Playfair+Display" rel="stylesheet">

		<!-- Font Awesome-->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
			$( window ).on( "load", function() {
				$('.full').hide();
    		});
		</script>
		
		<script>
			var food = ['Hamburger', 'Piadina', 'Insalta', 'Lasagne', 'Sandwich'];
			var count = 1;
			window.setInterval(function(){
				jQuery(function($) {
					if (!$(".form-control-search").is(":focus")) {
						$('.form-control-search').attr("placeholder", food[count]);
						count = (count + 1) % 5;
					}
				});
			}, 1000);
		</script>
		
		<?php if(isset($_GET['login'])) : ?>
			<script>
				$(window).on( "load", function() {
					snackBar("Login effettuato con successo.");
	    		});
			</script>
		<?php endif; ?>

		<?php if(isset($_GET['success'])) : ?>
			<script>
				$(window).on( "load", function() {
					snackBar("Utente creato con successo.");
	    		});
			</script>
		<?php endif; ?>
	</head>
	
	<body>
		<div class="full"><div id="loader"></div></div>