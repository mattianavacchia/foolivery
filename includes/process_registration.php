<?php
include 'config.php';
include 'functions.php';

// Recupero la password criptata dal form di inserimento.
$password = $_POST['p'];
$email = $_POST['email'];
$username = $_POST['generalita'];
$type = $_POST['type'];

// Crea una chiave casuale
$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
// Crea una password usando la chiave appena creata.
$password = hash('sha512', $password.$random_salt);
// Inserisci a questo punto il codice SQL per eseguire la INSERT nel tuo database
// Assicurati di usare statement SQL 'prepared'.
if ($insert_stmt = $mysqli->prepare("INSERT INTO members (username, email, password, salt, type) VALUES (?, ?, ?, ?, ?)")) {
   $insert_stmt->bind_param('sssss', $username, $email, $password, $random_salt, $type);
   // Esegui la query ottenuta.
   if($insert_stmt->execute()) {
	   header('Location: ../login.php?registration=1');
   } else {
	   header('Location: ../registration.php?error=1');
   }
}

?>