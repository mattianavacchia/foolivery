<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<?php if(login_check($mysqli) == true && ( check_type(1) )) : ?>

	<section id="manage-menu" class="text-center">
		<h1>Gestisci menu Fornitori</h1>
		<div class="container">
			<div class="row">
				<table class="table">
					<thead>
						<tr>
							<th id="#" scope="col">#</th>
							<th id="generalita" scope="col">Generalità</th>
							<th id="email" scope="col">Email</th>
							<th id="gestisci" scope="col">Gestisci</th>
				    	</tr>
				  	</thead>
				  	<tbody>
					<?php
						
						$query_sql="SELECT * FROM members WHERE type = 3";
						$result = $mysqli->query($query_sql);
						if ($result !== false) {
							if ($result->num_rows > 0) {
								while($row = $result->fetch_assoc()) {?>
										
									<tr>
								    	<td headers="id" scope="row"><?php echo($row["id"]) ?></th>
									    <td headers="generalita"><?php echo($row["username"]) ?></td>
									    <td headers="email"><?php echo($row["email"]) ?></td>
									    <td headers="gestisci"><p class="text-uppercase btn-manage btn-manage-supplier"><a href="manage-menu.php?id=<?php echo($row["id"]) ?>&nome=<?php echo($row["username"]) ?>">GESTISCI</a></p></td>
							    	</tr>
										
									<?php
								}
							}
						}
							
					?>
				  	</tbody>
				</table>
			</div>
		</div>
	</section>

	<script src="assets/js/sha512.js"></script>
	<script src="assets/js/forms.js"></script>

<?php else: ?>

	<section id="manage-menu" class="text-center">
		<h1>Accesso Negato</h1>
		<p>Non hai un account adeguato per poter visualizzare questa pagina.</p>
	</section>

<?php endif; ?>

<?php include("includes/footer.php"); ?>
