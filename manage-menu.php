<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<?php if(login_check($mysqli) == true && ( check_type(3) || check_type(1) )) : ?>

	<?php 
		if(isset($_GET["id"])) {
			$write_fornitore = true;
			$id_fornitore_passed = $_GET["id"];
			$name = $_GET["nome"];
		} else {
			$write_fornitore = false;
			$id_fornitore_passed = $_SESSION['user_id'];
			$name = $_SESSION['username'];
		}
	?>

	<!-- content -->
	<section id="manage-menu">
		<h1 class="text-center">Menu di <?php echo($name); ?></h1>
		<div class="container">
			<div class="row text-center" style="background: #e9e9e9; padding: 15px 30px 30px;">
				<p class="text-uppercase">Comandi rapidi:</p><br/>
				<button class="change-state" id="add-new-plate" data-toggle="modal" data-target="#manage-menu-add">Aggiungi un nuovo Piatto</button>
				<button <?php if ($write_fornitore == true) { echo('data-check="true"'); echo('data-supplier="' . $id_fornitore_passed . '"'); } else { echo("data-check=false");  } ?> class="delete" id="delete-all-plates">Elimina tutti i Piatti</button>
			</div>
			<div class="row" id="plates">
				<?php
					$query_sql="SELECT * FROM pietanza WHERE id_fornitore = " . $id_fornitore_passed;
					$result = $mysqli->query($query_sql);
					if ($result !== false) {
						if ($result->num_rows > 0) {
							while($row = $result->fetch_assoc()) {?>
								<div class="col-md-4 menu-dish" data-id="<?php echo $row["id"]; ?>">
						    		<div class="dish text-center">
					                    <img src=<?php echo $row["url_immagine"];?> alt="">
					                    <div class="dish-info">
					                        <p class="dish-name"><?php echo $row["nome"]; ?></p>
					                    </div>
					                    <button data-id="<?php echo $row["id"]; ?>" data-price="<?php echo $row["prezzo"]; ?>" data-type="<?php echo $row["pietanza_tipo"]; ?>" data-name="<?php echo $row["nome"]; ?>" data-description="<?php echo $row["descrizione"]; ?>" class="text-uppercase btn-manage btn-manage-modify" data-toggle="modal" data-target="#manage-menu-add"><i class="glyphicon glyphicon-edit"></i> Modifica</button> 
					                    <button <?php if ($write_fornitore == true) { echo('data-check="true"'); echo('data-supplier="' . $id_fornitore_passed . '"'); } else { echo("data-check=false");  } ?> data-id="<?php echo $row["id"]; ?>" class="text-uppercase btn-manage btn-manage-remove"><i class="glyphicon glyphicon-remove"></i> Elimina</button>  
					                </div>
						    	</div>
							<?php }
						} else { ?>
							<p class="text-center" style="font-size: 2em;">Non ci sono pietanze da gestire.</p>
						<?php 
						}
					}
				?>
			</div>
		</div>
		
		<!-- Modal -->
		<div class="modal fade" id="manage-menu-add" tabindex="-1" role="dialog" aria-labelledby="manage-menu-add" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Aggiungi nuovo piatto</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
			      <form id="ajax-menu" method="post" action="./api/api-manage-menu-add.php" enctype="multipart/form-data">
		            <div>
		                <p><label for="nome"><strong>Nome</strong>
		                	<input type="text" placeholder="Inserisci Nome" name="nome" id="nome" required>
		                </label></p>
		                
		                <p><label for="descrizione"><strong>Descrizione</strong>
		                <input type="text" placeholder="Inserisci Descrizione" name="descrizione" id="descrizione" required>
		                </label></p>
		                
		                <p><label for="type"><strong>Tipologia</strong>
			                <select id="type" size="4" required>
								<option value="1">Carne</option>
								<option value="2">Pesce</option>
								<option value="3">Vegetariano</option>
								<option value="4">Vegano</option>
							</select>
						</label></p>
		                
		                <p><label for="prezzo"><strong>Prezzo</strong>
		                	<input type="number" placeholder="30" name="prezzo" id="prezzo" required><br/>
		                </label></p>
		
		                <p><label for="immagine" style="margin-top: 10px; margin-bottom: 10px;"><strong>Immagine Pietanza</strong>
		                	<input type="file" name="immagine" id="immagine" accept="image/x-png,image/gif,image/jpeg" required/>
		                </label></p>
		                
		                <button <?php 
		                		if ($write_fornitore == true) { 
		                			echo('data-check="true"'); 
		                			echo('data-supplier="' . $id_fornitore_passed . '"'); 
		                		} else { 
		                			echo("data-check=false");  
		                		} ?> 
		                	id="btn-complete-modal" class="btn-login" type="submit">Registra</button>
		            </div>
		        </form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
		      </div>
		    </div>
		  </div>
		</div>
	</section>

<?php else: ?>

	<section id="manage-menu" class="text-center">
		<h1>Accesso Negato</h1>
		<p>Non hai un account adeguato per poter visualizzare questa pagina.</p>
	</section>

<?php endif; ?>

<?php include("includes/footer.php"); ?>
