<?php include("includes/header.php"); ?>

		<?php if(isset($_SESSION['type'])) : ?>
			<?php header('Location: ./profile.php'); ?>
		<?php endif; ?>

		<script src="assets/js/sha512.js"></script>
		<script src="assets/js/forms.js"></script>
		<?php if(isset($_GET['error'])) : ?>
			<?php echo('<script>
						$(document).ready(function(){
						  $(".error").show();
						});
					 </script>');?>
		<?php endif; ?>

		<div class="container">
			<div class="row error">
				<p class="text-center">Errore nella fase di Registrazione. Ritenta!</p>
			</div>
		    <div class="row container-form">
		        <form class="form-login" action="./includes/process_registration.php" method="post" name="registration_form">
		            <div class="imgcontainer">
		                <img src="images/specials/img_avatar2.png" alt="Avatar" class="avatar">
		            </div>

		            <div>
		                <p><label for="email"><strong>Email</strong>
		                	<input type="text" placeholder="Inserisci Email" name="email" id="email" required>
		                </label></p>

		                <p><label for="password"><strong>Password</strong>
		                	<input type="password" placeholder="Inserisci Password" id="password" name="p" required>
		                </label></p>

		                <p><label for="generalita"><strong>Generalità</strong>
		                	<input type="text" placeholder="Inserisci Generalità" name="generalita" id="generalita" required>
		                </label></p>

		                <p><label for="type"><strong>Tipologia di account</strong>
			                <select name="type" id="type" size="2" required>
			                    <option value="2">Cliente</option>
			                    <option value="3">Fornitore</option>
			                </select>
		                </label></p>

		                <input class="btn-login" type="button" value="Registrati" onclick="formhash(this.form, this.form.password);" />
		            </div>

		            <span class="psw">Hai gia un <a href="login.php">account?</a></span>
		        </form>
		    </div>
		</div>


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="dist/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
	</body>
</html>
