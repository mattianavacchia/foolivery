<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<?php if(login_check($mysqli) == true && check_type(3)) : ?>

	<section>
		<div class="container">
			<div class="row">
				<h1 class="text-center">Ordini di <?php echo($_SESSION["username"]); ?></h1>
				<div class="row text-center" style="background: #e9e9e9; padding: 15px 30px 30px;">
					<p class="text-uppercase">Comandi rapidi:</p>
					<button class="change-state">Cambia stato a tutti 'In Lavorazione'</button>
					<button class="change-state">Cambia stato a tutti 'In Consegna'</button>
					<button class="change-state">Cambia stato a tutti 'Consegnato'</button>
					<button id="delete-all-notifications" class="delete">Elimina Tutte</button>
				</div>
				
				<div style="margin-bottom: 30px;" id="notification-center" class="row">
					<?php
						$query_sql = "SELECT ordine.id_ordine, ordine_pietanze.qnt, pietanza.nome, members.username, ordine.id_stato FROM ordine, ordine_pietanze, pietanza, members WHERE ordine.id_ordine = ordine_pietanze.id_ordine AND ordine_pietanze.id_pietanza = pietanza.id AND members.id = ordine.id_cliente AND pietanza.id_fornitore = " .$_SESSION["user_id"];
						$result = $mysqli->query($query_sql);
						if ($result != false) {
							while($row = $result->fetch_assoc()) {
								$arrays[] = $row["id_ordine"];
								
								switch($row["id_stato"]) {
									case "0":
										$status = "n-in-lav";
										$status_txt = "In Lavorazione";
										break;
									case "1":
										$status = "n-in-con";
										$status_txt = "In Consegna";
										break;
									case "2":
										$status = "n-checked";
										$status_txt = "Consegnato";
										break;
								}
								
								?>
	
							<div id="<?php echo($row["id_ordine"]); ?>" class="notification <?php echo($status); ?>" role="alert">
								<p>Ordine del cliente: <?php echo($row["username"]); ?> <u><b>(<?php echo($status_txt); ?>)</b></u></p>
								<p><?php echo($row["nome"]); ?> per una quantità di: <?php echo($row["qnt"]); ?></p>
								<label>Cambia Stato:</label>
								<select id="state<?php echo($row["id_ordine"]); ?>">
									<option value="0">In Lavorazione</option>
									<option value="1">In Consegna</option>
									<option value="2">Consegnato</option>
								</select>
								<button data-id="<?php echo($row["id_ordine"]); ?>" class="change-state">Conferma cambiamento Stato</button>
								<button data-id="<?php echo($row["id_ordine"]); ?>" class="delete">Elimina Notifica</button>
							</div>
								
							<?}
						}
					?>
					<?php
						for($i = 0; $i < sizeof($arrays); $i++) {
							$query_sql = "UPDATE ordine SET visitato = 1 WHERE id_ordine = " .$arrays[$i];
							$result = $mysqli->query($query_sql);
						}
					?>
				</div>
			</div>
		</div>
	</section>
	
<?php else: ?>

	<section id="manage-menu" class="text-center">
		<h1>Accesso Negato</h1>
		<p>Non hai un account adeguato per poter visualizzare questa pagina.</p>
	</section>
	
<?php endif; ?>

<?php include("includes/footer.php"); ?>
