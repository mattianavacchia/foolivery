<?php include("includes/header.php"); ?>

		<?php if(isset($_SESSION['type'])) : ?>
			<?php header('Location: ./profile.php'); ?>
		<?php endif; ?>

		<script src="assets/js/sha512.js"></script>
		<script src="assets/js/forms.js"></script>
		<?php if(isset($_GET['error'])) : ?>
			<?php echo('<script>
						$(document).ready(function(){
						  $(".error").show();
						});
					 </script>');?>
		<?php endif; ?>
		<?php if(isset($_GET['registration'])) : ?>
			<?php echo('<script>
						$(document).ready(function(){
						  $(".success").show();
						});
					 </script>');?>
		<?php endif; ?>

		<div class="container">
			<div class="row error">
				<p class="text-center">Errore nella fase di Login. Ritenta!</p>
			</div>
			<div class="row success">
				<p class="text-center">Registrazione effettuata con successo. Effettua il Login!</p>
			</div>
		    <div class="row container-form">
		        <form class="form-login" action="./includes/process_login.php" method="post" name="login_form">
		            <div class="imgcontainer">
		                <img src="images/specials/img_avatar2.png" alt="Avatar" class="avatar">
		            </div>
		
		            <div>
		                <p><label for="email"><strong>Email</strong>
			                <input type="text" placeholder="Inserisci Email" name="email" id="email" required>
		                </label></p>
		
		                <p><label for="password"><strong>Password</strong>
			                <input type="password" placeholder="Inserisci Password" name="p" id="password" required>
		                </label></p>
		                    
		                <input class="btn-login" type="button" value="Login" onclick="formhash(this.form, this.form.password);" />
		            </div>
		            <span class="psw">Non hai un account? <a href="./registration.php">Registrati!</a></span>
		        </form>
		    </div>
		</div>

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
		<script src="dist/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
	</body>
</html>