<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<!-- content -->
<?php $message = ""; ?>
<?php if(isset($_SESSION['type'])) : ?>

	<?php
		
		if (isset($_POST["generalita"]) && isset($_POST["email"]) && isset($_POST["optConsegna"])) {
			$generalita = $_POST["generalita"];
			$email = $_POST["email"];
			$consegna = $_POST["optConsegna"];
			
			if ($consegna == "Altro:") {
				if (isset($_POST["input_zona"])) {
					$consegna = $_POST["input_zona"];
				} else {
					$message = "Per indicare un'altra zona il campo di testo deve essere compilato.";
				}
			}
			
			$consegna = str_replace("'", "\'", $consegna);
			$query_sql="INSERT INTO ordine (dataOra, luogo_consegna, id_cliente, id_stato) VALUES('" . date('Y-m-d H:i:s') ."', '" . $consegna . "', '" . $_SESSION["user_id"] . "', '0')";
			if ($mysqli->query($query_sql) === TRUE) {
			    $last_id = $mysqli->insert_id;
			    
			    foreach($_SESSION["shopping_cart"] as $keys => $values) {
				    $query_sql="INSERT INTO ordine_pietanze (id_ordine, id_pietanza, qnt) VALUES('" . $last_id ."', '" . $values["product_id"] . "', '" . $values["product_quantity"] . "')";
				    
				    if ($mysqli->query($query_sql) === TRUE) {
					    $message = "Grazie per aver effettuato l'ordine da noi. Ti arriveranno notifiche sullo stato del tuo ordine.";
					    // svuota carrello
					    unset($_SESSION["shopping_cart"]);
				    } else {
					    $message = "Ci sono stati problemi nel processare il tuo ordine. Riprova.";
				    }
				}
			} else {
			    $message = "Ci sono stati problemi nel processare il tuo ordine. Riprova.";
			}
		} else {
			$message = "Dati mancanti o errati. Ricompilare.";
		}
		
	?>
	
<?php else: ?>
	<?php $message = "Non hai i permessi per accedere a questa area." ?>
<?php endif; ?>

<section>
	<div class="container">
		<div class="row">
			<h1 class="text-center" style="font-size: 3em; padding-bottom: 30px;"><?php echo($message); ?></h1>
		</div>
	</div>
</section>

<?php include("includes/footer.php"); ?>
