<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<?php if(login_check($mysqli) == true && check_type(3)) : ?>

	<?php $array_of_indices = array(); ?>

	<section>
		<h1 class="text-center">Ordini di <?php echo($_SESSION["username"]); ?></h1>
		<div class="container">
			<div class="row">
				
				<div style="margin-bottom: 30px;" id="notification-center" class="row">
					<?php 
						
						$query_sql = "SELECT ordine.id_ordine, ordine_pietanze.qnt, pietanza.nome, members.username, ordine.id_stato FROM ordine, ordine_pietanze, pietanza, members WHERE ordine.id_ordine = ordine_pietanze.id_ordine AND ordine_pietanze.id_pietanza = pietanza.id AND members.id = ordine.id_cliente AND pietanza.id_fornitore = " .$_SESSION["user_id"]. " GROUP BY ordine.id_ordine, ordine_pietanze.qnt, pietanza.nome, members.username, ordine.id_stato";
						$result = $mysqli->query($query_sql);
						if ($result != false) {
							while($row = $result->fetch_assoc()) {
								
								$arrays[] = $row["id_ordine"];
								
								switch($row["id_stato"]) {
									case "0":
										$status = "n-in-lav";
										$status_txt = "In Lavorazione";
										break;
									case "1":
										$status = "n-in-con";
										$status_txt = "In Consegna";
										break;
									case "2":
										$status = "n-checked";
										$status_txt = "Consegnato";
										break;
								}
								
								?>
								
								<div id="<?php echo($row["id_ordine"]); ?>" class="notification <?php echo($status); ?>" role="alert">
									<p>Ordine del cliente: <?php echo($row["username"]); ?> <u><strong>(<?php echo($status_txt); ?>)</strong></u></p>
									<ol style="list-style-type: square; margin-bottom: 5px; margin-top: 5px; margin-left: 3%; color: #fff;">
								
								<?php
								array_push($array_of_indices, $row["id_ordine"]);
								$query_sql_inside = "SELECT ordine_pietanze.id_ordine, pietanza.nome, ordine_pietanze.qnt FROM ordine_pietanze, pietanza WHERE ordine_pietanze.id_pietanza = pietanza.id AND ordine_pietanze.id_ordine = " .$row["id_ordine"] ;
								$result_inside = $mysqli->query($query_sql_inside);
								if ($result_inside != false) {
									while($row_inside = $result_inside->fetch_assoc()) {
										?>
											<li><strong><?php echo($row_inside["nome"]); ?></strong> per una quantità di: <?php echo($row_inside["qnt"]); ?></li>										
										<?php
									}
								}
								?>
									</ol>
									<form>
										<p><label for="state<?php echo($row["id_ordine"]); ?>">Cambia Stato:
											<select id="state<?php echo($row["id_ordine"]); ?>" style="color: #000 !important;">
												<option value="0">In Lavorazione</option>
												<option value="1">In Consegna</option>
												<option value="2">Consegnato</option>
											</select>
										</label></p>
										
									</form>
									<button data-id="<?php echo($row["id_ordine"]); ?>" class="change-state">Conferma cambiamento Stato</button>
								</div>
							<?php }
						}
					?>
					<?php
						foreach($array_of_indices as $single) {
							$query_sql = "UPDATE ordine SET visitato = 1 WHERE id_ordine = " . $single;
							$result = $mysqli->query($query_sql);
						}
					?>
				</div>
			</div>
		</div>
	</section>
	
<?php else: ?>

	<section id="manage-menu" class="text-center">
		<h1>Accesso Negato</h1>
		<p>Non hai un account adeguato per poter visualizzare questa pagina.</p>
	</section>
	
<?php endif; ?>

<?php include("includes/footer.php"); ?>
