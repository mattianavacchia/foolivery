function type_from_number(type) {
	var type_s = "";
	switch (type) {
		case "1": type_s = "Admin"; break;
		case "2": type_s = "Cliente"; break;
		case "3": type_s = "Fornitore"; break;
		case "4": type_s = "Fattorino"; break;
		default: type_s = "Tipo non definito"; break;
	}
	
	return type_s;
}

$(document).ready(function() {
	
	$('.btn-manage-remove').click(function() {
		
		$.getJSON('./api/api-manage-users.php?request=removeuser&id=' + $(this).data("id"), function(data) {
			var html_code = "";
			
			location.reload();
			/*for(var i = 0; i < data.length; i++){
				html_code += '<tr><th scope="row">' + data[i]["id"] + '</th><td>' + data[i]["username"] + '</td><td>' + data[i]["email"] + '</td><td>' + type_from_number(data[i]["type"]) + '</td><td><p data-id="' + data[i]["id"] + '" class="text-uppercase btn-manage btn-manage-remove"><i class="glyphicon glyphicon-remove"></i> Elimina</p></td></tr>';
			}
			
			$("table tbody").html(html_code);*/
		});
	});
	
	var form = $('#ajax-contact');
	$(form).submit(function(event) {
		console.log('save');
	    // Stop the browser from submitting the form.
	    event.preventDefault();
	
	    // TODO
	    var formData = $(form).serialize();
	    
	    // Submit the form using AJAX.
		$.ajax({
		    type: 'POST',
		    url: $(form).attr('action'),
		    data: formData
		}).done(function(response) {
		    // Make sure that the formMessages div has the 'success' class.
		    
		    snackBar("Utente aggiunto con successo.");
		    document.location.reload(true);
		
		    // Clear the form.
		    $('#name').val('');
		    $('#email').val('');
		    $('#generalita').val('');
		}).fail(function(data) {
		    // Make sure that the formMessages div has the 'error' class.
		    //alert('Errore');
		    snackBar("Errore nell'aggiungere un nuovo fattorino.");
		});
	});
	
});

