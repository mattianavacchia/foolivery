var count_orders = 0;

var ajax_call = function() {
	$(document).ready(function(){
		$.ajax({
			url: './api/api-notifications.php',
			method:"POST",
			dataType:"json",
			success:function(data){
				if(data.result != null && data.result.length > 0 && count_orders < data.result.length) {
					if(data.user == "F") {
						snackBar('Ci sono nuovi ordini da controllare.');
						$('#order_count').text(data.result.length);
					} else {
						snackBar('Ci sono nuove notifiche da controllare.');
						$('#notification_count').text(data.result.length);
						// ricarico se vogliamo
					}
					count_orders = data.result.length;
				}
			}
		});
	});
};

var interval = 1000 * 60 * 0.05;

setInterval(ajax_call, interval);
ajax_call();

$(document).ready(function(){
	
	$('#notification-center .delete').click(function(){
		var id = $(this).attr("data-id");
		$.ajax({
			url: './api/api-notifications-actions.php?action=removeSingle&id=' + id,
			method:"GET",
			dataType:"json",
			success:function(data){
				if (data.success == true) {
					snackBar(data.message);
					$('div#single-notification[data-id="' + id + '"]').hide();
				} else {
					snackBar("Errore nell'effettuare l'operazione. Ricaricare la pagina e ritentare.");
				}
			}
		});
	});
	
	$('#notification-center .delete-order').click(function() {
		var id = $(this).attr("data-id");
		$.ajax({
			url: './api/api-notifications-actions.php?action=removeSingleOrder&id=' + id,
			method:"GET",
			dataType:"json",
			success:function(data){
				if (data.success == true) {
					snackBar(data.message);
					$('div#' + id).hide();
				} else {
					snackBar("Errore nell'effettuare l'operazione. Ricaricare la pagina e ritentare.");
				}
			}
		});
	});
	
	$('#notification-center .change-state').click(function(){
		var id = $(this).attr("data-id");
		var selector = "#state" + id;
		var state = $(selector).children("option:selected").val();
		console.log('./api/api-notifications-actions.php?action=changeState&id=' + id + '&state=' + state);
		$.ajax({
			url: './api/api-notifications-actions.php?action=changeState&id=' + id + '&state=' + state,
			method:"GET",
			dataType:"json",
			success:function(data){
				if (data.success == true) {
					var state_css = "";
					switch(state) {
						case "0":
							state_css = "n-in-lav";
							break;
						case "1":
							state_css = "n-in-con";
							break;
						case "2":
							state_css = "n-checked";
							break;
					}
					
					snackBar(data.message);
					$('div#' + id).removeClass();
					$('div#' + id).addClass("notification");
					$('div#' + id).addClass(state_css);
				} else {
					snackBar("Errore nell'effettuare l'operazione. Ricaricare la pagina e ritentare.");
				}
			}
		});
	});
	
	$('#delete-all-notifications').click(function(){
		$.ajax({
			url: './api/api-notifications-actions.php?action=removeAll',
			method:"GET",
			dataType:"json",
			success:function(data){
				if (data.success == true) {
					snackBar(data.message);
					$('#notification-center').hide();
				} else {
					snackBar("Errore nell'effettuare l'operazione. Ricaricare la pagina e ritentare.");
				}
			}
		});
	});
});