$(document).ready(function(){
	
	load_cart_data();
	
	function load_cart_data() {
		$.ajax({
			url: './api/cart/fetch_cart.php',
			method:"POST",
			dataType:"json",
			success:function(data){
				$('#cart_item_count').text(data.total_item);
				$('#cart_details').html(data.cart_details);
				$('#row-cart-page').html(data.cart_details);
			}
		});
	}
	
	$(document).on('click', '.add_to_cart', function() {
		var product_id = $(this).attr("id");
		var product_name = $(this).data("name");
		var product_price = $(this).data("price");
		var product_quantity = 1;
		var action = "add";
		console.log(product_id);
		console.log(product_name);
		console.log(product_price);
		console.log(product_quantity);
		$.ajax({
			url:"./api/cart/actions_cart.php",
			method:"POST",
			data:{product_id:product_id, product_name:product_name, product_price:product_price, product_quantity:product_quantity, action:action},
			success:function(data) {
				load_cart_data();
				snackBar("Pietanza aggiunta al carrello.");
			}
		});
	});
	
	$(document).on('click', '.delete-product', function(){
		var product_id = $(this).attr("id");
		var action = 'remove';
		if(confirm("Sei sicuro/a di voler eliminare questo prodotto?")) {
			$.ajax({
				url:"./api/cart/actions_cart.php",
				method:"POST",
				data:{product_id:product_id, action:action},
				success:function()
				{
					load_cart_data();
					snackBar("Pietanza rimossa dal carrello.");
				}
			})
		}
		else
		{
			return false;
		}
	});

	$(document).on('click', '#clear_cart', function(){
		var action = 'empty';
		$.ajax({
			url:"./api/cart/actions_cart.php",
			method:"POST",
			data:{action:action},
			success:function()
			{
				load_cart_data();
				snackBar("Carrello svuotato.");
			}
		});
	});
	
});