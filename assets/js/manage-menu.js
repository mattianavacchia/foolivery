$(document).ready(function() {

	
	var form = $('#ajax-menu');
	var id;
	var price;
	var description;
	var type;
	var name;
	$(form).submit(function(event) {
		// Stop the browser from submitting the form.
	    event.preventDefault();
	    
	    var name = $('input#nome').val();
	    var description = $('input#descrizione').val();
	    var type = $('select#type').val();
	    var price = $('input#prezzo').val();
	    var image = $('input#immagine').val().replace(/C:\\fakepath\\/i, '')
	    console.log(image);

	    var url_final = '';
	    var check = $('#btn-complete-modal').attr("data-check");
	    var isTrueSet = (check == 'true');
	    var supplier = $('#btn-complete-modal').attr("data-supplier");
	    if (isTrueSet) {
		    url_final = './api/api-manage-menu-add.php?nome=' + name + '&descrizione=' + description + '&tipo=' + type + '&prezzo=' + price + '&immagine=' + image +'&fornitore=' + supplier;
	    } else {
		    url_final = './api/api-manage-menu-add.php?nome=' + name + '&descrizione=' + description + '&tipo=' + type + '&prezzo=' + price + '&immagine=' + image;
	    }

	    if($('#btn-complete-modal').text() == 'Modifica') {
		    url_final = './api/api-manage-menu-modify.php?nome=' + name + '&descrizione=' + description + '&tipo=' + type + '&prezzo=' + price + '&immagine=' + image + '&idpiatto=' + $('#btn-complete-modal').attr("data-id-piatto");
	    }

	    console.log("FILE UPLOADING START");
	    var formData = new FormData(this);
	    console.log(formData);
	    formData.append("type", type);

	    $.ajax({
	        url: './upload.php',
	        type: 'POST',
	        data: formData,
	        success: function (data) {
	            console.log(data)
	        },
	        cache: false,
	        contentType: false,
	        processData: false
	    });

	    console.log("FILE UPLOADING END");

	    

	    
	    console.log(url_final);
	    	    
	    // Submit the form using AJAX.
		$.ajax({
		    type: 'GET',
		    url: url_final,
		    data: 'json'
		}).done(function(response) {
			var json_r = JSON.parse(response);
		    $('#manage-menu-add').modal('toggle');
		    snackBar("Piatto aggiunto/modificato alla raccolta.");
		    // add plate
		    if($('#btn-complete-modal').text() != 'Modifica') {
			    var new_element = '<div class="col-md-4 menu-dish" data-id="' + json_r.id + '"><div class="dish text-center"><img src="./images/menu-img/' + image + '" alt=""><div class="dish-info"><p class="dish-name">' + name + '</p></div><button data-id="' + json_r.id + '" data-price="' + price + '" data-type="' + type + '" data-name="' + name + '" data-description="' + description + '" class="text-uppercase btn-manage btn-manage-modify" data-toggle="modal" data-target="#manage-menu-add"><i class="glyphicon glyphicon-edit"></i> Modifica</button> <button';
			    if(check == true) {
				    new_element += ' data-check="true" data-supplier="' + supplier + '"';
			    } else {
				    new_element += ' data-check="false"';
			    }
			    new_element += 'data-id="'+ json_r.id +'" class="text-uppercase btn-manage btn-manage-remove"><i class="glyphicon glyphicon-remove"></i> Elimina</button></div></div>';
			    $('#plates:last-child').append(new_element);
			} else {
				var menu = $('.menu-dish[data-id="' + id + '"]');
				menu.find('.dish-name').text(name);
				menu.find('img').val(img);
				menu.find('button').attr('data-description').text(description);
			}
		}).fail(function(data) {
			snackBar("Impossibile aggiungere il piatto. Riprova più tardi.");
		});
	});
	
	$('.btn-manage-modify').click(function() {
		id = $(this).attr("data-id");
		name = $(this).attr("data-name");
		price = $(this).attr("data-price");
		description = $(this).attr("data-description");
		type = $(this).attr("data-type");

		$('input#nome').val($(this).attr("data-name"));
		$('input#descrizione').val($(this).attr("data-description"));
		$('input#prezzo').val($(this).attr("data-price"));
		$('input#type').val(type);
		$('h5#exampleModalLabel').text("Modifica piatto esistente");
		$('#btn-complete-modal').text("Modifica");
		$('#btn-complete-modal').attr("data-id-piatto", id);
	});
	
	$('#add-new-plate').click(function() {
		$('h5#exampleModalLabel').html("Aggiungi nuovo piatto");
		$('#btn-complete-modal').text("Registra");
	});
	
	$('.btn-manage-remove').click(function() {
		console.log("rmeove");
		var id = $(this).attr("data-id");
		$.ajax({
			url: './api/api-manage-menu-remove.php?id=' + id,
			method:"GET",
			dataType:"json",
			success:function(data){
				if (data.success == true) {
					snackBar(data.message);
					$('div[data-id=' + id + ']').hide();
				} else {
					snackBar("Errore nell'effettuare l'operazione. Ricaricare la pagina e ritentare.");
				}
			}
		});
	});
	
	$('#delete-all-plates').click(function() {
		var url_final = '';
	    var check = $(this).attr("data-check");
	    var isTrueSet = (check == 'true');
	    if (isTrueSet) {
		    url_final = './api/api-manage-menu-remove.php?fornitore=' + $(this).attr("data-supplier");
	    } else {
		    url_final = './api/api-manage-menu-remove.php';
	    }
		$.ajax({
			url: url_final,
			method:"GET",
			dataType:"json",
			success:function(data){
				if (data.success == true) {
					snackBar(data.message);
					$('#plates').hide();
				} else {
					snackBar("Errore nell'effettuare l'operazione. Ricaricare la pagina e ritentare.");
				}
			}
		});
	});
	
});