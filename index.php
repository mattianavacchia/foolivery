<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<!-- content -->
<section class="most-sold-plates">
    <div class="container">
        <div class="row">
            <div class="col-12 menu-heading">
                <div class="col-12">
                    <div class="section-heading text-center">
                        <h1>Piatti scelti da noi</h1>
                    </div>
                </div>
                <div class="col-12 text-center">
                    <!-- btn -->
                    <a href="menu.php" class="btn most-sold-plates-btn"><span></span> Scopri l'intero Menu</a>
                </div>
            </div>
        </div>

        <div class="row">
            <?php
                $query_sql="SELECT * FROM pietanza ORDER BY RAND() LIMIT 3";
                $result = $mysqli->query($query_sql);
                if ($result !== false) {
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {?>

                        <div class="col-12 col-sm-6 col-md-4">
                <div class="dish text-center">
                    <img src="images/menu-img/dish-1.png" alt="Piatto del mese.">
                    <div class="dish-info">
                        <h2 class="dish-name"><?php echo $row["nome"]; ?> <span class="dish-price">$<?php echo $row["prezzo"]; ?></span></h2>
                    </div>
                </div>
            </div>

                        <?php
                        }
                    }
                }

            ?>
        </div>
    </div>
</section>

<section id="testimonials">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <i class="fas fa-pen-nib fa-4x"></i><h1>Su di noi</h1>
                </div>
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Carousel indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for carousel items -->
                    <div class="carousel-inner">
                        <div class="item carousel-item active">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <a>
                                                <img src="images/specials/img_avatar2.png" alt="Avatar testimonial.">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="testimonial">
                                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit.</p>
                                                <p class="overview"><strong>Paula Wilson</strong></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <a>
                                                <img src="images/specials/img_avatar2.png" alt="Avatar testimonial.">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="testimonial">
                                                <p>Vestibulum quis quam ut magna consequat faucibus.</p>
                                                <p class="overview"><strong>Antonio Moreno</strong></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item carousel-item">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <a>
                                                <img src="images/specials/img_avatar2.png" alt="Avatar testimonial.">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="testimonial">
                                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit.</p>
                                                <p class="overview"><strong>Michael Holz</strong></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <a>
                                                <img src="images/specials/img_avatar2.png" alt="Avatar testimonial.">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="testimonial">
                                                <p>Vestibulum quis quam ut magna consequat faucibus.</p>
                                                <p class="overview"><strong>Mary Saveley</strong></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item carousel-item">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <a>
                                                <img src="images/specials/img_avatar2.png" alt="Avatar testimonial.">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="testimonial">
                                                <p>Lorem ipsum dolor sit amet, consec adipiscing elit.</p>
                                                <p class="overview"><strong>Martin Sommer</strong></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="media">
                                        <div class="media-left d-flex mr-3">
                                            <a>
                                                <img src="images/specials/img_avatar2.png" alt="Avatar testimonial.">
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <div class="testimonial">
                                                <p>Vestibulum quis quam ut magna consequat faucibus.</p>
                                                <p class="overview"><strong>John Williams</strong></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="how-to-order text-center">
    <i class="fas fa-shopping-cart fa-4x"></i><h1>Semplice e sicuro</h1>
    <div class="container">
        <div id="myCarouselSempliceSicuro" class="carousel slide" data-ride="carousel">

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="images/dish1-slider.png" alt="Los Angeles" style="width:100%;">
                </div>

                <div class="item">
                    <img src="images/dish2-slider.png" alt="Chicago" style="width:100%;">
                </div>

                <div class="item">
                    <img src="images/dish3-slider.png" alt="New york" style="width:100%;">
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarouselSempliceSicuro" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarouselSempliceSicuro" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>

<?php include("includes/footer.php"); ?>