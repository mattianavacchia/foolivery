<?php include("includes/header.php"); ?>
<?php include("includes/header-content.php"); ?>

<?php if(login_check($mysqli) == true && ( check_type(2) )) : ?>
<?php $arrays_id_orders = array(); ?>

	<!-- content -->
	<section>
		<h1 class="text-center">Notifiche</h1>
		<div class="container">
			<div class="row">
				<div style="margin-bottom: 30px;" id="notification-center" class="row">
					<?php
						$query_sql = "SELECT ordine.id_ordine FROM ordine WHERE ordine.id_cliente = " .$_SESSION['user_id'];
						$result = $mysqli->query($query_sql);
						if ($result != false) {
							while($row = $result->fetch_assoc()) {
								array_push($arrays_id_orders, $row['id_ordine']);
							}
						}

						foreach ($arrays_id_orders as $single_id) {
							?>
							<div id="<?php echo($row["id_ordine"]); ?>" class="notification <?php echo($status); ?>" role="alert">
								<p>Ordine del cliente <?php echo $single_id; ?></p>
								<ol style="list-style-type: square; margin-bottom: 5px; margin-top: 5px; margin-left: 3%; color: #fff;">
									<?
									$query_sql = "SELECT pietanza.nome, ordine_pietanze.qnt, members.username FROM ordine_pietanze, pietanza, members WHERE
																ordine_pietanze.id_pietanza = pietanza.id
																AND pietanza.id_fornitore = members.id
																AND ordine_pietanze.id_ordine = " .$single_id;
									$result = $mysqli->query($query_sql);
									if ($result != false) {
										while($row = $result->fetch_assoc()) {?>
											// show results
											<li><strong><?php echo($row_inside["nome"]); ?></strong> per una quantità di: <?php echo($row_inside["qnt"]); ?> da: <?php echo($row_inside["username"]); ?></li>
										<?}?>
									</ol>
									<?php
									} else {?>
										<li>Ordine non più disponibile a causa della cancellazione di una o più pietanze che erano presenti.</li>
									<?}
								}?>
					<?php
						$query_sql = "UPDATE notifiche_fattorino SET checked = 1 WHERE id_fattorino = " . $_SESSION["user_id"];
						$result = $mysqli->query($query_sql);
					?>
				</div>
			</div>
		</div>
	</section>

<?php else: ?>

	<section id="manage-menu" class="text-center">
		<h1>Accesso Negato</h1>
		<p>Non hai un account adeguato per poter visualizzare questa pagina.</p>
	</section>

<?php endif; ?>

<?php include("includes/footer.php"); ?>
