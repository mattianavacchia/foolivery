<?php include('../includes/config.php'); ?>
<?php include('../includes/functions.php'); ?>
<?php sec_session_start(); ?>
<?php

    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
	    
	    $nome = $_GET['nome'];
		$descrizione = $_GET['descrizione'];
		$prezzo = $_GET['prezzo'];
		$pietanza_tipo = $_GET['tipo'];
		
		$uploaddir = './images/menu-img/';
		$uploadfile = $uploaddir . $_GET['immagine'];
		if(isset($_GET["fornitore"])) {
			$fornitore = $_GET["fornitore"];
		} else {
			$fornitore = $_SESSION['user_id'];
		}
		
		if ($insert_stmt = $mysqli->prepare("INSERT INTO pietanza (prezzo, nome, pietanza_tipo, descrizione, url_immagine, id_fornitore) VALUES (?, ?, ?, ?, ?, ?)")) {
		   $insert_stmt->bind_param('ssssss', $prezzo, $nome, $pietanza_tipo, $descrizione, $uploadfile, $fornitore);
		   // Esegui la query ottenuta.
		   if($insert_stmt->execute()) {
			   $result = array(
				   "success" => true,
				   "id" => $insert_stmt->insert_id
			   );
		   } else {
			   $result = array(
				   "success" => true
			   );
		   }
		}
		
		echo (json_encode($result));
		
    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "There was a problem with your submission, please try again.";
    }

?>