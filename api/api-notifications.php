<?php include('../includes/config.php'); ?>
<?php include('../includes/functions.php'); ?>
<?php sec_session_start(); ?>

<?php

$output = array();
	
	if(check_type(3)) {
		// fornitore
		$query_sql = "SELECT * FROM ordine, ordine_pietanze, pietanza WHERE ordine.id_ordine = ordine_pietanze.id_ordine AND ordine_pietanze.id_pietanza = pietanza.id AND pietanza.id_fornitore = " .$_SESSION["user_id"] . " AND visitato = 0 ORDER BY ordine.dataOra DESC";
		$user = "F";
	} else if(check_type(2)) {
		// cliente
		$query_sql = "SELECT * FROM notifiche_cliente WHERE id_cliente = " .$_SESSION["user_id"] . " AND checked = 0 ORDER BY id_notifica DESC";
		$user = "C";
	} else {
		// fattorino
		$query_sql = "SELECT * FROM notifiche_fattorino WHERE id_fattorino = " .$_SESSION["user_id"] . " AND checked = 0 ORDER BY id_notifica DESC";
		$user = "C";
	}
	
	if(login_check($mysqli) == true) {
		$result = $mysqli->query($query_sql);
		if ($result != false) {
			while($row = $result->fetch_assoc()) {
				$output[] = $row;
			}
		} else {
			$output = array('result' => 'false');
		}
	}
	
	$final_output = array(
		'user' => $user,
		'result' => $output
	);
	
	print json_encode($final_output);

?>