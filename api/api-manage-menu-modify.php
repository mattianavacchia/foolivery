<?php include('../includes/config.php'); ?>
<?php include('../includes/functions.php'); ?>
<?php sec_session_start(); ?>
<?php
    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "GET") {
	    
	    $nome = $_GET['nome'];
		$descrizione = $_GET['descrizione'];
		$prezzo = $_GET['prezzo'];
		$pietanza_tipo = $_GET['tipo'];
		$id_piatto = $_GET["idpiatto"];
		$uploaddir = './images/menu-img/';
        $uploadfile = $uploaddir . $_GET['immagine'];
		
		if ($insert_stmt = $mysqli->prepare("UPDATE pietanza SET prezzo = ?, nome = ?, pietanza_tipo = ?, descrizione = ?, url_immagine = ? WHERE id = ?")) {
		   $insert_stmt->bind_param('ssssss', $prezzo, $nome, $pietanza_tipo, $descrizione, $uploadfile, $id_piatto);
		   // Esegui la query ottenuta.
		   if($insert_stmt->execute()) {
			   $result = array(
				   "success" => true,
				   "id" => $mysqli->insert_id
			   );
		   } else {
			   $result = array(
				   "success" => false
			   );
		   }
		}
		
		echo (json_encode($result));
		
    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "There was a problem with your submission, please try again.";
    }

?>