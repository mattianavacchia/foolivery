<?php include('../includes/config.php'); ?>
<?php include('../includes/functions.php'); ?>
<?php sec_session_start(); ?>
<?php
	
	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		
		if(isset($_GET["id"])) {
			// singolo
			$query_sql = "DELETE FROM pietanza WHERE id = " .$_GET["id"];
		} else{
			if(isset($_GET["fornitore"])) {
				$fornitore = $_GET["fornitore"];
			} else {
				$fornitore = $_SESSION['user_id'];
			}
			$query_sql = "DELETE FROM pietanza WHERE id_fornitore = " .$fornitore;
		} 
		
		$result = $mysqli->query($query_sql);
		if ($result != false) {
			$message = "Piatto/i rimosso/i con successo.";
			print json_encode(array(
				'success' => true,
				'message' => $message
			));
		} else {
			print json_encode(array(
				'success' => false
			));
		}
	}
	
?>