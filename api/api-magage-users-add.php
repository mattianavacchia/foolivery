<?php include('../includes/config.php'); ?>
<?php include('../includes/functions.php'); ?>
<?php sec_session_start(); ?>
<?php

    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
	    
	    $password = $_POST['p'];
		$email = $_POST['email'];
		$username = $_POST['generalita'];
		$type = $_POST['type'];
		
		$random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
		$password = hash('sha512', $password.$random_salt);
		
		if($type != 4) {
			if ($insert_stmt = $mysqli->prepare("INSERT INTO members (username, email, password, salt, type) VALUES (?, ?, ?, ?, ?)")) {
			   $insert_stmt->bind_param('sssss', $username, $email, $password, $random_salt, $type);
			   // Esegui la query ottenuta.
			   if($insert_stmt->execute()) {
				   $output = array(
				   		'result' => 'true',
				   		'id' => $mysqli->insert_id,
				   		'username' => $username,
				   		'email' => $email,
				   		'type' => 'type'
				   );
			   } else {
				   $output = array('result' => 'false');
			   }
			}
		} else {
			$query_sql="INSERT INTO members (username, email, password, salt, type, id_fornitore) VALUES ('". $username ."', '". $email ."', '". $password ."', '". $random_salt ."', '". $type ."', '". $_SESSION['user_id'] ."')";
			$result = $mysqli->query($query_sql);
			if($result !== false) {
			   $output = array(
			   		'result' => 'true',
			   		'id' => $mysqli->insert_id,
			   		'username' => $username,
			   		'email' => $email,
			   		'type' => 'type'
			   );
		    } else {
			   $output = array('result' => 'false');
		    }

		}
		echo json_encode($output);

  		if ($_SESSION['type'] != 1) {
		 header('Location: ../manage-bellboy.php?success=1');
		} else {
			header('Location: ../manage-users.php?success=1');
		}
		
    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "There was a problem with your submission, please try again.";
    }

?>