<?php include('../includes/config.php'); ?>
<?php include('../includes/functions.php'); ?>
<?php sec_session_start(); ?>
<?php

	if ($_SERVER["REQUEST_METHOD"] == "GET") {
		$action = $_GET["action"];

		if(check_type(3)) {
			// fornitore
			$user = "F";
		} else {
			// cliente
			$user = "C";
		}

		switch($action) {
			case "removeSingle":
				$id = $_GET["id"];
				if(check_type(4)) {
					// fattorino
					$query_sql = "DELETE FROM notifiche_fattorino WHERE id_notifica = " .$id;
				} else {
					// cliente
					$query_sql = "DELETE FROM notifiche_cliente WHERE id_notifica = " .$id;
				}
				$result = $mysqli->query($query_sql);
				if ($result != false) {
					$message = "Notifica rimossa con successo.";
					print json_encode(array(
						'success' => true,
						'message' => $message
					));
				}
				break;
			case "removeSingleOrder":
				$id = $_GET["id"];
				$query_sql_1 = "DELETE FROM ordine WHERE id_ordine = " .$id;
				$result_1 = $mysqli->query($query_sql_1);
				$query_sql_2 = "DELETE FROM ordine_pietanze WHERE id_ordine = " .$id;
				$result_2 = $mysqli->query($query_sql_2);
				if ($result_1 != false && $result_2 != false) {
					$message = "Ordine rimosso con successo.";
					print json_encode(array(
						'success' => true,
						'message' => $message
					));
				}
				break;
			case "removeAll":
				if(check_type(4)) {
					// fattorino
					$query_sql = "DELETE FROM notifiche_fattorino WHERE id_fattorino = " .$_SESSION["user_id"];
				} else {
					// cliente
					$query_sql = "DELETE FROM notifiche_cliente WHERE id_cliente = " .$_SESSION["user_id"];
				}
				$result = $mysqli->query($query_sql);
				if ($result != false) {
					$message = "Notifiche rimosse con successo.";
					print json_encode(array(
						'success' => true,
						'message' => $message
					));
				}
				break;
			case "changeState":
				$id = $_GET["id"];
				$state = $_GET["state"];

				/* NOTIFICA A CLIENTE */
				$id_cliente = "";
				$id_fattorino = "";
				$query_sql = "SELECT id_cliente FROM ordine WHERE id_ordine = " .$id;
				$result = $mysqli->query($query_sql);
				if($result != false) {
					while($row = $result->fetch_assoc()) {
						$id_cliente = $row["id_cliente"];
					}
					$query_sql = "INSERT INTO notifiche_cliente (id_cliente, testo, id_stato) VALUES ('". $id_cliente ."', 'Lo stato del tuo ordine e' stato cambiato', '". $state ."')";
					$result = $mysqli->query($query_sql);
				}
				/* NOTIFICA A FATTORINO */
				if($state == 1) {
					$query_sql = "SELECT id FROM members WHERE id_fornitore = " . $_SESSION["user_id"] . " ORDER BY rand() LIMIT 1";
					$result = $mysqli->query($query_sql);
					if($result != false) {
						while($row = $result->fetch_assoc()) {
							$id_fattorino = $row['id'];
							// devo notificare il cliente che deve consegnare
							$query_sql = "INSERT INTO notifiche_fattorino (id_fattorino, id_ordine) VALUES ('". $id_fattorino ."', '". $id ."')";
							$result2 = $mysqli->query($query_sql);
						}
					}
				}

				$query_sql = "UPDATE ordine SET id_stato = " . $state . " WHERE id_ordine = " .$id;
				$result = $mysqli->query($query_sql);
				if ($result != false) {
					$message = "Stato dell'ordine aggiornato con successo.";
					print json_encode(array(
						'success' => true,
						'message' => $message
					));
				}
		}
	}

?>
